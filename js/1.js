// 1.Document Object Model (DOM) - це структура, що використовується для представлення 
// веб-сторінок в браузері.дозволяє JavaScript отримувати доступ до цих елементів, змінювати 
// їх властивості та взаємодіяти з ними.

// 2.Різниця між властивостями HTML-елементів innerHTML та innerText полягає в тому, що innerHTML 
// повертає HTML-код всередині елемента, включаючи теги, тоді як innerText повертає лише текстовий вміст 
// елемента, без HTML-тегів.

// 3.Кращий спосіб залежить від конкретного випадку. Якщо вам потрібно звернутися до конкретного елемента 
// з унікальним ідентифікатором, то метод getElementById() є швидким і простим способом. Якщо ви шукаєте 
// групу елементів з однаковим класом або тегом, то краще використовувати методи getElementsByClassName() 
// або getElementsByTagName(). Якщо вам потрібно знайти елементи, які відповідають певному селектору CSS,
//  то методи querySelector() та querySelectorAll() є корисними.


const paragraphs = document.getElementsByTagName("p");
for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = "#ff0000";
}

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
const parentElement = optionsList.parentElement;
console.log(parentElement);
if (optionsList.hasChildNodes()) {
  const childNodes = optionsList.childNodes;
  for (let i = 0; i < childNodes.length; i++) {
    console.log(childNodes[i].nodeName + ": " + childNodes[i].nodeType);
  }
}

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = 'This is a paragraph';

const header = document.querySelector('.main-header');
const navItems = header.querySelectorAll('*');
navItems.forEach(item => item.classList.add('nav-item'));
console.log(navItems);

const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(title => title.classList.remove('section-title'));




